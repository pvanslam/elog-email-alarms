FROM python:3

WORKDIR /app

RUN pip3 install lxml requests passlib pandas beautifulsoup4 imap_tools msal https://github.com/paulscherrerinstitute/py_elog/archive/1.3.9.zip

COPY runner.py runner.py
RUN mkdir /data && chown 10001:10001 /data
USER 10001

CMD ["python3", "runner.py"]